import nltk
import re
from nltk import word_tokenize
from nltk import pos_tag
from rake_nltk import Rake ##phrases
import pandas as pd
import math


print("########## START ##########")

###############################
## functions
###############################

def get_pos (sentence):
	## getting POS
	tkns = word_tokenize(sentence)
	tags = pos_tag(tkns)
	#print (tags)
	return tags

def get_start_pos (st_pos_file):
	## POS rules for start of the headline
	start_pos = []
	with open(st_pos_file,'r') as fs:
		stph = fs.readlines()
		for ss in stph:
			start_pos.append(ss.rstrip('\n').split(', '))

	#print ("---start_pos: ",start_pos)
	return start_pos

def get_power_words (pw_wd_file):
	## power words for headline
	power_words = []
	with open(pw_wd_file,'r') as fs:
		stph = fs.readlines()
		for ss in stph:
			power_words = power_words + (ss.rstrip('\n').split(', '))

	#print ("---power_words: ",power_words)
	return power_words


def is_number(s):
	## checking if token is a number
	if '%' in s:
		s = s.replace('%', '')

	try:
		float(s)
		return True
	except ValueError:
		pass

	try:
		import unicodedata
		unicodedata.numeric(s)
		return True
	except (TypeError, ValueError):
		pass

	return False


def break_headline_rake(headline):

	#r = Rake(stopwords=[], punctuations='.,-:;|!\\n', ranking_metric= None)
	#punctuations='.,-:;|!\\n'

	parts = re.split(r'[`\-=~!@#^*()\[\]{};\'\\:"|<./<>?]', headline)

	#r.extract_keywords_from_text(headline)

	#ranked_phrases = r.get_ranked_phrases()
	#print(ranked_phrases) # To get keyword phrases ranked highest to lowest.

	clean_ranked_phrases = []

	for ph in parts:
		try:
			ss = ph.strip()
			clean_ranked_phrases.append(ss)
		except:
			print("Empty String")

	clean_ranked_phrases = list(filter(None, clean_ranked_phrases))
	#print(clean_ranked_phrases)
	return clean_ranked_phrases


def key_phrases_rake(headline):
	## extracting key words and key phrases form the headline
	r = Rake()
	r.extract_keywords_from_text(headline)
	ranked_phrases = r.get_ranked_phrases()

	clean_ranked_phrases = []

	for ph in ranked_phrases:
		try:
			ss = ph.strip()
			clean_ranked_phrases.append(ss)
		except:
			print("Empty String")

	#print(clean_ranked_phrases)
	return clean_ranked_phrases


def split_headline(headline):
	## breaking headline into word string and character string
	wrd_list = headline.split(' ')
	char_list = headline.split()

	return wrd_list, char_list


def get_start_phrase_score(st_fb_file):
	## pre calculated start phrase scores
	start_phrases_score = {}
	with open(st_fb_file,'r') as fs:
		stph = fs.readlines()
		for ss in stph:
			#print(ss)
			st = ss.split(' ')
			#print(st)
			#print(ss[0])
			num = float(st[0])
			#print(asdasd )
			start_phrases_score[(' '.join(st[1:])).rstrip('\n')] = num
	return start_phrases_score


def get_start_word_score(st_wd_file):
	## pre calculated start word scores
	start_word_score = {}
	with open(st_wd_file,'r') as fs:
		stph = fs.readlines()
		for ss in stph:
			#print(ss)
			st = ss.split(' ')
			#print(st)
			#print(ss[0])
			num = float(st[0])
			#print(asdasd )
			start_word_score[(st[1]).rstrip('\n')] = num
	return start_word_score


def get_pop_trigram_score(tri_fb_file):
	## pre calculated trigram scores
	tri_phrases_score = {}
	with open(tri_fb_file,'r') as fs:
		stph = fs.readlines()
		for ss in stph:
			#print(ss)
			st = ss.split(' ')
			#print(st)
			#print(ss[0])
			num = float(st[0])
			#print(asdasd )
			tri_phrases_score[(' '.join(st[1:])).rstrip('\n')] = num
	
	return tri_phrases_score


def get_headline_length_score(min_length, max_length, max_len_score):
	# headline length restriction
	#TODO improving distribution over headline length
	len_score = {}
	for i in range(1,max_length+3):
		sc = math.sqrt(abs(i - ((min_length+max_length+1)/2.0)))
		if sc!=0:
			sc = max_len_score/sc
		else:
			sc = max_len_score
		len_score[str(i)] = sc
	return len_score


def get_context_words(ct_wd_file_time, ct_wd_file_insight, ct_wd_file_relativ):
	## getting context words
	context_words_time = []
	with open(ct_wd_file_time,'r') as fs:
		lst = fs.readlines()
		for ss in lst:
			context_words_time.append(ss.rstrip('\n'))

	context_words_insight = []
	with open(ct_wd_file_insight,'r') as fs:
		lst = fs.readlines()
		for ss in lst:
			context_words_insight.append(ss.rstrip('\n'))

	context_words_relativ = []
	with open(ct_wd_file_relativ,'r') as fs:
		lst = fs.readlines()
		for ss in lst:
			context_words_relativ.append(ss.rstrip('\n'))

	return context_words_time, context_words_insight, context_words_relativ

def get_power_word_score(headline, power_words):
	## giving power words score
	pw_score = 0.0
	pw_comments = []
	fl = True
	for pw in power_words:
		if pw in headline:
			pw_score += 8
			fl = False
			pw_comments.append("Wow! you are using power words, Cameron approves it (y)")
	
	if fl:
			pw_comments.append('Try using Power Words in your headline to make it more engaging')

	return pw_score, pw_comments


def analyse_headline_start(headline_start):
	## TODO including varitions in the start phrase scores using better POS scoring
	#print(headline_start)
	# headline start length score
	st_score = 0.0
	st_comments = []

	# length analysis
	max_len = 5
	min_len = 2
	max_st_score = 3
	headline_start_words = headline_start.split(' ')
	st_len = len(headline_start_words)
	if st_len>=2 and st_len<=5:
		st_score += max_st_score-abs(max_st_score - st_len)
	else:
		st_comments.append("Try to keep the starting phrase between 2-5 words")

	# validating the length
	if(st_len<=0):
		st_comments.append("Length of the starting phrase is not valid")
		return st_score, st_comments

	# POS analysis
	useful_start_pos = ['CD', 'RB', 'RBR', 'RBS', 'VB', 'JJ', 'PRP', 'PRP$', 'WDT', 'WP', 'WP$', 'WRB', 'NN', 'NNS', 'NNP', 'NNPS', 'RP', 'SYM', '.', '$']
	# getting pos
	pos_tags = get_pos(headline_start)
	#print(pos_tags)
	pos_as_list = []
	for posTag in pos_tags:
		pos_as_list.append(posTag[1])
		if posTag[1] in useful_start_pos:
			st_score += 1
	#print ("---- pos_as_list: ", pos_as_list)
	## start posr rules
	st_pos_file = '/Users/sachindeora/Codes/data_science/headline_analysis/sample_score_engagnement/words_phrases_score/headline_start/phrase_pos.txt'
	start_phrases_pos = get_start_pos(st_pos_file)

	## start phrase of the headline scores
	st_fb_file = '/Users/sachindeora/Codes/data_science/headline_analysis/sample_score_engagnement/words_phrases_score/headline_start/start_trigram_score.txt'
	start_phrases_score = get_start_phrase_score(st_fb_file)

	## start word of the headline scores
	st_wd_file = '/Users/sachindeora/Codes/data_science/headline_analysis/sample_score_engagnement/words_phrases_score/headline_start/first_word_score.txt'
	start_word_score = get_start_word_score(st_wd_file)

	## cameron power words/phrases
	pw_wd_file = '/Users/sachindeora/Codes/data_science/headline_analysis/sample_score_engagnement/words_phrases_score/headline_start/cameron_power_words.txt'
	power_words = get_power_words(pw_wd_file)

	## replacing numeric values to 'x'
	for i in range(0,st_len):
		#print (headline_start_words[i])
		if is_number(headline_start_words[i]):
			headline_start_words[i] = 'x'
			#print("****found a number***")
			st_score += 1
			st_comments.append("Cool!! Inclusion of numeric value in headline is very popular")

	if st_len>2:
		fl = True
		if ' '.join(headline_start_words[:3]) in start_phrases_score.keys():
			fl = False
			st_score +=  start_phrases_score[ ' '.join(wrd_list[:3])]
		if fl:
			## checking if pos are right
			if pos_as_list in start_phrases_pos:
				st_score += 4
	
	if headline_start_words[0] in start_word_score.keys():
			st_score += start_word_score[headline_start_words[0]]

	## giving power words score
	pw_score, pw_comments = get_power_word_score(headline_start, power_words)
	st_score += pw_score
	st_comments += pw_comments

	return st_score, st_comments


def get_trigram_score(headline):
	## other trigram phrases score
	tri_score = 0.0
	tri_comments = []

	## popular trigram scores
	tri_fb_file = '/Users/sachindeora/Codes/data_science/headline_analysis/sample_score_engagnement/words_phrases_score/others/trigram_score.txt'
	tri_phrases_score = get_pop_trigram_score(tri_fb_file)

	#print (tri_phrases_score.keys())

	for ks in tri_phrases_score.keys():
		if ks in headline:
			tri_score = tri_score + tri_phrases_score[ks]

	return tri_score, tri_comments


def get_context_words_score(headline_words):
	## context words score
	#context words
	##TODO handling * words
	ct_wd_file_time = '/Users/sachindeora/Codes/data_science/headline_analysis/sample_score_engagnement/words_phrases_score/others/context_words_time.txt'
	ct_wd_file_insight= '/Users/sachindeora/Codes/data_science/headline_analysis/sample_score_engagnement/words_phrases_score/others/context_words_insight.txt'
	ct_wd_file_relativ = '/Users/sachindeora/Codes/data_science/headline_analysis/sample_score_engagnement/words_phrases_score/others/context_words_relativ.txt'
	context_words_time, context_words_insight, context_words_relativ = get_context_words(ct_wd_file_time,ct_wd_file_insight,ct_wd_file_relativ)
	
	cw_score = 0.0
	cw_comments = []
	
	ct = 0 #time word count
	for wd in headline_words:
		if wd in context_words_time and ct <3:
			cw_score = cw_score + 2  ##2 is just hard coded for now
			ct += 1
		if ct>=3:
			break;
	
	ci = 0 #insite word count
	for wd in headline_words:
		if wd in context_words_insight and ci <3:
			cw_score = cw_score + 2  ##2 is just hard coded for now
			ci += 1
		if ci>=3:
			break;
	
	cr = 0 #relative word count
	for wd in headline_words:
		if wd in context_words_relativ and cr <3:
			cw_score = cw_score + 2  ##2 is just hard coded for now
			cr += 1
		if cr>=3:
			break;

	if cr and ci and ct:
		cw_comments.append('Awesome!! Having multiple context words in your headline improves its quality')

	elif (cr and ci) or (cr and ct) or (ct and ci):
		cw_comments.append('Great!! context words word count in your headline is good')
	elif cr or ct or ci:
		cw_comments.append('Right on Track!! Context word in your headline makes it better')
	else:
		cw_comments.append('Try to use context words in your headlines to make it more attractive')


	return cw_score, cw_comments


###############################
# headline length restriction
#TODO improving distribution over headline length
min_length = 5
max_length = 12
max_len_score = 6
#distributing score (keeping gaussian distribution)

len_score = get_headline_length_score(min_length, max_length, max_len_score)

###############################
## processing headline
###############################

#headline = '50 % Off!! stylish shoes on discount - myntra.com'
#headline = 'Non Resident Travel Cover. | For Visitors to Australia‎'
#headline = 'hurry up!! this will make you bring project end soon'
#headline = 'Renew Your Car Insurance | Save Upto 70% On Premium | coverfox.com‎'
#headline = 'Beat the June 30 Deadline and Save On Tax!'
#headline = 'Beat the June 30 Deadline | Save On Tax!'
#headline = 'Members Own Health Insurance | 18 Not For Profit Health Funds'
#headline = 'Family Health Insurance | Compare & Join In 5 Minutes'
#headline = 'Black & White Cabs - Book Now | Fast & Reliable Service'
# headline = 'Book a Black & White Cab Now | Fast & Reliable Service'
headline = 'Get A Free Quote Today | Health Insurance for Pregnancy'
## lowercase
headline =  headline.lower()

## final score of the headline
final_score = 0.0
## final comments on the headlines
headline_comments = []

headline_parts = break_headline_rake(headline)
headline_keywords = key_phrases_rake(headline)
headline_words, headline_char = split_headline(headline) ## try splitting parts of headline

headline_length = len(headline_words)


###############################
## scoring headline
###############################

## length score
if str(headline_length) in len_score.keys():
	final_score = final_score + len_score[str(headline_length)]
	#headline_comments.append("")
else:
	headline_comments.append("Try to keep your headline words length between 5-12")

###############################
## start of the headline score
headline_start = ''
if len(headline_parts)>1:
	print("Headline Parts:")
	print (headline_parts)
	headline_start = headline_parts[0]
else:
	## TODO improving for better extractions
	# manually getting start of the headline
	if headline_length>3:
		headline_start = ' '.join(headline_words[:4])
	else:
		headline_start = headline

st_score, st_comments = analyse_headline_start(headline_start)

print ("Start_score: ", st_score)
final_score += st_score
headline_comments += st_comments


###############################

## other trigram phrases score
tri_score, tri_comments = get_trigram_score(headline)

final_score += tri_score
headline_comments += tri_comments

## context words score
cw_score, cw_comments = get_context_words_score(headline_words)

final_score += cw_score
headline_comments += cw_comments

###############################
print(final_score)
print(headline_comments)



print("########## FINISH ##########")
