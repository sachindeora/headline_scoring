import pandas as pd
import nltk
import math



print("########## START ##########")

#########
#facebook
#########

f = 'most_facebook_engagements.txt'
pop_phrases = []
total_sum = 0.0
with open(f) as fs:
	#print(fs)
	lines = fs.readlines()
	#print(lines)
	for line in lines:
		#print(line)
		l = line.split(' ')
		num = math.log(int(l[0]))
		#print(num)
		total_sum = total_sum + num
		pop_phrases.append((' '.join(l[1:]), num))

#print(len(pop_phrases))

print(total_sum)
top_score = []
for ph in pop_phrases:
	sc = int(ph[1])/total_sum
	top_score.append((ph[0],sc*100))


f = 'lowest_facebook_engagements.txt'
flop_phrases = []
#total_sum = 0.0
with open(f,'r') as fs:
	lines = fs.readlines()
	for line in lines:
		l = line.split(' ')
		num = math.log(int(l[0]))
		#total_sum = total_sum + int(num)
		flop_phrases.append((' '.join(l[1:]), num))

#print(len(flop_phrases))


flop_score = []
for ph in flop_phrases:
	sc = int(ph[1])/total_sum
	flop_score.append((ph[0],sc*100))

fr = 'trigram_score.txt'

file = open(fr,'w')

for ss in top_score:
	file.write(str(ss[1])+ ' ' + ss[0])
file.write('\n')
for ss in flop_score:
	file.write(str(ss[1])+ ' ' + ss[0])

file.close()

#print(total_sum)
#file_write = []

#print (top_score)
#print (flop_score)

#####################################
#most popular first words_facebook
#####################################
# f = 'most_popular_first_words_facebook_refined.txt'
# pop_words = []
# total_sum = 0.0
# with open(f) as fs:
# 	#print(fs)
# 	lines = fs.readlines()
# 	#print(lines)
# 	for line in lines:
# 		#print(line)
# 		l = line.split(' ')
# 		num = math.log(int(l[0]))
# 		#print(num)
# 		total_sum = total_sum + num
# 		pop_words.append((l[1], num))
# 		#pop_words.append((l[1],num))

# pop_score = []
# for pw in pop_words:
# 	sc = int(pw[1])/total_sum
# 	pop_score.append((pw[0],sc*100))
# #print(len(pop_phrases))

# fr = 'first_word_score.txt'

# file = open(fr,'w')

# for ss in pop_score:
# 	file.write(str(ss[1])+ ' ' + ss[0])



print("########## FINISH ##########")


