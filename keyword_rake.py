import nltk
from rake_nltk import Rake ##phrases

## splitting headline into parts and other useful extractions
print("########## START ##########")

#headline = '50% OFF!! stylish shoes on discount - myntra.com'
#headline = 'Non Resident Travel Cover. | For Visitors to Australia‎'
headline = 'What are the best keyword extraction algorithms for NLP'
r = Rake()
r.extract_keywords_from_text(headline)
ranked_phrases = r.get_ranked_phrases()

clean_ranked_phrases = []

for ph in ranked_phrases:
	try:
		ss = ph.strip()
		clean_ranked_phrases.append(ss)
	except:
		print("Empty String")

#print(clean_ranked_phrases)

print(clean_ranked_phrases)

print("########## FINISH ##########")