import nltk
from rake_nltk import Rake ##phrases

## splitting headline into parts and other useful extractions
print("########## START ##########")

#headline = '50% OFF!! stylish shoes on discount - myntra.com'
headline = 'Non Resident Travel Cover. | For Visitors to Australia‎'
break_symbols = [',','.','-','!!!','!!','!','|',':',';','?']

break_indexes = []
for sb in break_symbols:
	ix = 0
	try:
		ix = headline.index(sb)
	except:
		ix = -1
	if ix >= 0:
		break_indexes.append(ix)

print (break_indexes)

r = Rake(stopwords=[], punctuations='.,-:;|!\\n')

r.extract_keywords_from_text(headline)

ranked_phrases = r.get_ranked_phrases()
print(ranked_phrases) # To get keyword phrases ranked highest to lowest.

clean_ranked_phrases = []

for ph in ranked_phrases:
	try:
		ss = ph.strip()
		clean_ranked_pjrases.append(ss)
	except:
		print("Empty String")

print(clean_ranked_phrases)

print("########## FINISH ##########")


