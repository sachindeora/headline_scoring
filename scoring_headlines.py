import pandas as pd
import nltk
import math




print("########## START ##########")

headline = 'hurry up!! this will make you bring project end soon'

final_score = 0.0

###############################
#start of the headline scores
st_fb_file = 'start_trigram_score.txt'
start_phrases_score = {}
with open(st_fb_file,'r') as fs:
	stph = fs.readlines()
	for ss in stph:
		#print(ss)
		st = ss.split(' ')
		#print(st)
		#print(ss[0])
		num = float(st[0])
		#print(asdasd )
		start_phrases_score[(' '.join(st[1:])).rstrip('\n')] = num

#print(start_phrases)
st_wd_file = 'first_word_score.txt'
start_word_score = {}

with open(st_wd_file,'r') as fs:
	stph = fs.readlines()
	for ss in stph:
		#print(ss)
		st = ss.split(' ')
		#print(st)
		#print(ss[0])
		num = float(st[0])
		#print(asdasd )
		start_word_score[(st[1]).rstrip('\n')] = num


###############################
# trigram score
tri_fb_file = 'trigram_score.txt'
tri_phrases_score = {}
with open(tri_fb_file,'r') as fs:
	stph = fs.readlines()
	for ss in stph:
		#print(ss)
		st = ss.split(' ')
		#print(st)
		#print(ss[0])
		num = float(st[0])
		#print(asdasd )
		tri_phrases_score[(' '.join(st[1:])).rstrip('\n')] = num


###############################
# headline length restriction
#TODO improving distribution over headline length
min_length = 5
max_length = 12
max_score = 6
#distributing score (keeping gaussian distribution)

len_score = {}
for i in range(1,max_length+3):
	sc = math.sqrt(abs(i - ((min_length+max_length+1)/2.0)))
	if sc!=0:
		sc = max_score/sc
	else:
		sc = max_score
	len_score[str(i)] = sc

#print(len_score)

###############################
#context words
##TODO hadeling * words
ct_wd_file_time = 'context_words_time.txt'
context_words_time = []

with open(ct_wd_file_time,'r') as fs:
	lst = fs.readlines()
	for ss in lst:
		context_words_time.append(ss.rstrip('\n'))

ct_wd_file_insight= 'context_words_insight.txt'
context_words_insight = []

with open(ct_wd_file_insight,'r') as fs:
	lst = fs.readlines()
	for ss in lst:
		context_words_insight.append(ss.rstrip('\n'))

ct_wd_file_relativ = 'context_words_relativ.txt'
context_words_relativ = []

with open(ct_wd_file_relativ,'r') as fs:
	lst = fs.readlines()
	for ss in lst:
		context_words_relativ.append(ss.rstrip('\n'))

#print (context_words_time[:5])
###############################
#special symbols

spl_symbols = ['!', ',' '-', '&', '$', '.', '%']



###############################
## processing headline
###############################

wrd_list = headline.split(' ')
char_list = headline.split()

lenth = len(wrd_list)
## length score
if str(lenth) in len_score.keys():
	final_score = final_score + len_score[str(lenth)]

## starting words and phrases and score
if lenth>=3:
	if ' '.join(wrd_list[:3]) in start_phrases_score.keys():
		final_score = final_score + start_phrases_score[' '.join(wrd_list[:3])]

	elif wrd_list[0] in start_word_score.keys():
		final_score = final_score + start_word_score[wrd_list[0]]
else:
	if wrd_list[0] in start_word_score.keys():
		final_score = final_score + start_word_score[wrd_list[0]]

## other trigram phrases score
for ks in tri_phrases_score.keys():
	if ks in headline:
		final_score = final_score + tri_phrases_score[ks]

## context words score
ct = 0 #time word count
for wd in wrd_list:
	if wd in context_words_time and ct <3:
		final_score = final_score + 2  ##2 is just hard coded for now
		ct += 1
	if ct>=3:
		break;
ci = 0 #insite word count
for wd in wrd_list:
	if wd in context_words_insight and ci <3:
		final_score = final_score + 2  ##2 is just hard coded for now
		ci += 1
	if ci>=3:
		break;
cr = 0 #relative word count
for wd in wrd_list:
	if wd in context_words_relativ and cr <3:
		final_score = final_score + 2  ##2 is just hard coded for now
		cr += 1
	if cr>=3:
		break;
###############################
print(final_score)
print("########## FINISH ##########")

