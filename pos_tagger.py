import nltk
from nltk import word_tokenize
from nltk import pos_tag
 
#sentence = word_tokenize("Rami Eid is studying at Stony Brook University in NY")
#sentence = word_tokenize("get up by 20%")

#print (sentence)

#tags = pos_tag(sentence)

#print (tags)


hd_list = ['10 reasons why',
'10 things you',
'this is what',
'this is the',
'this is how',
'15 of the',
'12 ways to',
'this is why',
'the 10 best',
'how to make ',
'these are the',
'here are the',
'how to get',
'13 stunning photos',
'7 things that',
'3 things to',
'you can now',
'the 7 most',
'5 things only',
'why you should']

for st in hd_list:
	tkns = word_tokenize(st)
	tags = pos_tag(tkns)
	print (tags)

print('****** FINISHED ******')

